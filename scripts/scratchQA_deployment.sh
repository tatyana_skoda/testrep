#!/bin/bash

export DEV_QA_AUTH_URL=force://PlatformCLI::5Aep861CJdnCt1rNxU6AjxAMMG4xTUYJSyBJQwH14W_IlltGGPTQhBxA13yFC53qwZFjpY._zPHi7P6Z28QoqVB@java-power-4465-dev-ed.cs92.my.salesforce.com/

# install salesforce cli
wget https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
mkdir sfdx
tar xJf sfdx-linux-amd64.tar.xz -C sfdx --strip-components 1
"./sfdx/install"
export PATH=./sfdx/$(pwd):$PATH
sfdx --version
sfdx plugins --core

# authorize in the main dev org
echo $DEV_QA_AUTH_URL > dev_qa_auth_token
sfdx auth:sfdxurl:store -f dev_qa_auth_token -a devqa

# deploy merged changes into the main dev org
sfdx force:source:deploy -x ./manifest/package.xml -u devqa