#!/bin/bash

# export variables  
export SFDX_AUTOUPDATE_DISABLE=false
export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
export SFDX_DOMAIN_RETRY=300
export SFDX_DISABLE_APP_HUB=true
export SFDX_LOG_LEVEL=DEBUG
export DEPLOYDIR=src
export TESTLEVEL=RunLocalTests
export SFDX_AUTH_URL=force://PlatformCLI::5Aep861QT4b0TO85TNHSCKCj5b0PzgE455hAjUXTnngInqZAo4Hj.xr.nzAKgCZInV9IfgKK3gzv_29lwdYsHPg@cadalysinternal.my.salesforce.com

# install salesforce cli
wget https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
mkdir sfdx
tar xJf sfdx-linux-amd64.tar.xz -C sfdx --strip-components 1
"./sfdx/install"
export PATH=./sfdx/$(pwd):$PATH
sfdx --version
sfdx plugins --core


# authorize dev hub
echo $SFDX_AUTH_URL > auth_token
sfdx force:auth:sfdxurl:store -f auth_token -a devhub


# create a default scratch org
sfdx force:org:create -f ./config/project-scratch-def.json -a scratch -v devhub -d 1


# push sources to scratch org
sfdx force:apex:execute -u scratch -f ./scripts/enableKnowledgeUser.apex 
sfdx force:source:push -u scratch


# run unit tests
sfdx force:apex:test:run -r junit -y -c -l RunLocalTests -w 10 -u scratch


# delete scratch org
sfdx force:org:delete -u scratch -p