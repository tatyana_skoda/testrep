#!/bin/bash

# This script creates scratch org and enables Lightning Component debug mode for current useer
# Before run this script you need to authenticate sfdx cli against Dev Hub

if [ "$1" == "" ]; then
    echo "Please specify org alias"
    exit 1
fi

targetOrgAlias="$1"
sfdx force:org:create -f config/project-scratch-def.json --durationdays 10 -a "$targetOrgAlias" --setdefaultusername --wait 5
sfdx force:apex:execute -f ./scripts/enableLightningDebug.apex --targetusername "$targetOrgAlias"
sfdx force:apex:execute -f ./scripts/enableKnowledgeUser.apex --targetusername "$targetOrgAlias"