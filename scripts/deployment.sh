#!/bin/bash

export MAIN_DEV_AUTH_URL=force://PlatformCLI::5Aep861QbHyftz0nI9X_I8ZdLywTY9KSJilwZtNlpgMA25AYOWXs7VKF4NIYeMM7krTsnUlRv3dGoybuolEs.a3@ccon-dev-ed.my.salesforce.com

# authorize in the main dev org
echo $MAIN_DEV_AUTH_URL > main_dev_auth_token
sfdx force:auth:sfdxurl:store -f main_dev_auth_token -a maindev

# deploy merged changes into the main dev org
sfdx force:source:deploy -x ./manifest/package.xml -u maindev